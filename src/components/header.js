// import libraries
import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';

// make a component
const Header = (props) => {
    return (
        <View style={style.header}>
        <Text style={style.text}>{props.headerText}</Text>
        </View>
    );
};

const style = StyleSheet.create({
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5b51f',
        textAlign: 'center',
        height: 60,
        shadowColor: '#f5b51f',
        shadowOffset: {width : 0, height: 2},
        shadowOpacity: 0.5,
        elevation: 10,
        position: 'relative',
        color: '#002f5f'
    },
    text: {
        fontSize: 20,
        color: '#ffffff'
    }
})

// export this component that available for other component
export default Header;
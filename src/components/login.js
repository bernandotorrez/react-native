import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Button, Input } from 'nachos-ui'

const Login = () => {
  return (
      <View>
        <Input
          style={style.default}
          placeholder='Username'
        />
        <Input
          style={style.default}
          placeholder='Password'
        />
    	<Button type="danger" style={style.default}>Login</Button>
        </View>
  )
}

const style = StyleSheet.create({
    default: {
        margin: 15
    }
})

export default Login
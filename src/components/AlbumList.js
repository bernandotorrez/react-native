import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native'
import {Button, Card} from 'nachos-ui';
import axios from 'axios'

class AlbumList extends Component {

    constructor(props){
        super(props),
        this.state = {
            data: [],
            url: 'https://rallycoding.herokuapp.com/api/music_albums'
        }
    }

    componentWillMount(){
        let {url} = this.state

        axios.get(url)
        .then(response => this.setState({data: response.data}))

    }

    renderAlbum() {
        let {data} = this.state;

        return (
            data.map(album =>
               
                
                <Card key={album.title}
                    footerContent={album.artist+' - '+album.title}
                    style={style.card}
                    image={album.image}
                />
                
            )
        )
         
    }

    render () {
        
        return (
        <View style={style.view}>
            <Text style={style.text}>Album List</Text>

           {this.renderAlbum()}
            

            <Card

            />
        </View>
        )
        
    }
}

const style = StyleSheet.create({
    view: {
        color: '#002f5f',
        backgroundColor: '#002f5f',
        alignItems: 'center',
        paddingTop: 30
    },
    text: {
        fontSize: 20,
        margin: 15,
        color: 'white'
    },
    card: {
        margin: 15
    }
})

export default AlbumList;
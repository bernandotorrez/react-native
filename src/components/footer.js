import React from 'react';
import { Text, StyleSheet, View } from 'react-native'

const Footer = () => {
    return (
        <View style={style.view}>
        <Text style={style.text}>Copyright @ Bernando Torrez</Text>
        </View>
    )
}

const style = StyleSheet.create({
    view: {
        color: '#f5b51f',
        alignItems: 'center',
        flex: 1
    },
    text: {
        fontSize: 20
    }
})

export default Footer;
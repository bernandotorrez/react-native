/** @format */

import React from 'react'
import { AppRegistry, Platform, View } from 'react-native';
import { ThemeProvider } from 'nachos-ui';
import { name as appName } from './app.json';
import AlbumList from './src/components/AlbumList';
import Header from './src/components/Header';

const Layout = () => (
    
    <View style={style}>
        <ThemeProvider>
            <Header headerText={'Header'}/>
            <AlbumList/>
        </ThemeProvider>
    </View>
    
)

const style = {
    color: '#002f5f'
}

AppRegistry.registerComponent(appName, () => Layout);
